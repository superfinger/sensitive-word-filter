"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SensitiveWordFilter = void 0;
var SensitiveWordFilter = /** @class */ (function () {
    function SensitiveWordFilter(sensitiveWords) {
        //字典树
        this._trie = null;
        var root = new Map();
        for (var _i = 0, sensitiveWords_1 = sensitiveWords; _i < sensitiveWords_1.length; _i++) {
            var word = sensitiveWords_1[_i];
            var map = root;
            for (var i = 0; i < word.length; i++) {
                // 依次获取字
                var char = word.charAt(i);
                // 判断是否存在
                if (map.get(char)) {
                    // 获取下一层节点
                    map = map.get(char);
                }
                else {
                    // 将当前节点设置为非结尾节点
                    if (map.get('end') === 1) {
                        map.set('end', 0);
                    }
                    var item = new Map();
                    // 新增节点默认为结尾节点
                    item.set('end', 1);
                    map.set(char, item);
                    map = map.get(char);
                }
            }
        }
        this._trie = root;
    }
    /**
     * 检查敏感词是否存在
     */
    SensitiveWordFilter.prototype.check = function (input, startIndex) {
        var trieNode = this._trie, success = false, wordNum = 0, sensitiveWord = '', start = -1, end = -1;
        for (var i = startIndex; i < input.length; i++) {
            var word = input.charAt(i);
            trieNode = trieNode.get(word);
            if (trieNode) {
                if (start == -1) {
                    start = i;
                }
                wordNum++;
                sensitiveWord += word;
                if (trieNode.get('end') === 1) {
                    success = true; // 表示已到词的结尾
                    end = i;
                    break;
                }
            }
            else {
                break;
            }
        }
        // 两字成词
        if (wordNum < 2) {
            success = false;
            start = end = -1;
        }
        return { success: success, sensitiveWord: sensitiveWord, start: start, end: end };
    };
    // 过滤掉除了中文、英文、数字之外的
    SensitiveWordFilter.prototype.trim = function (input) {
        return input.replace(/[^\u4e00-\u9fa5\u0030-\u0039\u0061-\u007a\u0041-\u005a]+/g, '');
    };
    /**
     * 搜索文本中是否存在敏感词
     */
    SensitiveWordFilter.prototype.search = function (input) {
        var result;
        var content = this.trim(input);
        for (var i = 0, len = content.length; i < len; i++) {
            result = this.check(content, i);
            if (result.success) {
                break;
            }
        }
        return result;
    };
    SensitiveWordFilter.prototype.replace = function (input, replaceValue) {
        var result = this.search(input);
        if (result === null || result === void 0 ? void 0 : result.success) {
            var words = this.trim(input).split('');
            var trimWords = {};
            if (input.length > words.length) {
                for (var i = 0, len = input.length; i < len; i++) {
                    var char = input.charAt(i);
                    if (words.indexOf(char) == -1) {
                        trimWords[char] = i;
                    }
                }
            }
            if (!replaceValue) {
                replaceValue = '*';
            }
            for (var i = result.start; i <= result.end; i++) {
                words[i] = replaceValue;
            }
            if (Object.keys(trimWords).length > 0) {
                for (var char in trimWords) {
                    if (Object.prototype.hasOwnProperty.call(trimWords, char)) {
                        var index = trimWords[char];
                        words.splice(index, 0, char);
                    }
                }
            }
            return words.join('');
        }
        return input;
    };
    return SensitiveWordFilter;
}());
exports.SensitiveWordFilter = SensitiveWordFilter;
module.exports = { sensitiveWordFilter: SensitiveWordFilter };
