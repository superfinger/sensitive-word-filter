const fs = require('fs');
const path = require('path');
const { exit } = require('process');
const {sensitiveWordFilter} = require('../dist/index')


let file;
try {
	file = fs.readFileSync('test/bad-words.txt');
} catch (error) {
	console.error('找不到敏感词库');
	exit(1);
}

const words = file.toString().split('\n');
const filter = new sensitiveWordFilter(words)
console.log(filter.replace(`外　挂`))
